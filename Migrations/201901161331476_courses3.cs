namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class courses3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Courses", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Courses", new[] { "DepartmentId" });
            AddColumn("dbo.Courses", "BatchesId", c => c.Int(nullable: false));
            CreateIndex("dbo.Courses", "BatchesId");
            AddForeignKey("dbo.Courses", "BatchesId", "dbo.Batches", "Id", cascadeDelete: true);
            DropColumn("dbo.Courses", "DepartmentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "DepartmentId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Courses", "BatchesId", "dbo.Batches");
            DropIndex("dbo.Courses", new[] { "BatchesId" });
            DropColumn("dbo.Courses", "BatchesId");
            CreateIndex("dbo.Courses", "DepartmentId");
            AddForeignKey("dbo.Courses", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
        }
    }
}
