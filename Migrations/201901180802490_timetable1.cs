namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class timetable1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "WeekDay", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Timetables", "WeekDay");
        }
    }
}
