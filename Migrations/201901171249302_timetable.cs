namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class timetable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Timetables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BatchesId = c.Int(nullable: false),
                        Course = c.String(),
                        Room = c.String(),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Batches", t => t.BatchesId, cascadeDelete: true)
                .Index(t => t.BatchesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Timetables", "BatchesId", "dbo.Batches");
            DropIndex("dbo.Timetables", new[] { "BatchesId" });
            DropTable("dbo.Timetables");
        }
    }
}
