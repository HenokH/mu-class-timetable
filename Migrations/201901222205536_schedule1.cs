namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class schedule1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BatchesId = c.Int(nullable: false),
                        CourseId = c.Int(),
                        RoomsId = c.Int(nullable: false),
                        WeekDay = c.String(),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Batches", t => t.BatchesId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.Rooms", t => t.RoomsId, cascadeDelete: true)
                .Index(t => t.BatchesId)
                .Index(t => t.CourseId)
                .Index(t => t.RoomsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schedules", "RoomsId", "dbo.Rooms");
            DropForeignKey("dbo.Schedules", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Schedules", "BatchesId", "dbo.Batches");
            DropIndex("dbo.Schedules", new[] { "RoomsId" });
            DropIndex("dbo.Schedules", new[] { "CourseId" });
            DropIndex("dbo.Schedules", new[] { "BatchesId" });
            DropTable("dbo.Schedules");
        }
    }
}
