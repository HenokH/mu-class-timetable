namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class batchName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Batches", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Batches", "Name");
        }
    }
}
