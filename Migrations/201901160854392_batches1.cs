namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class batches1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Batches", "DepartmentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Batches", "DepartmentId");
            AddForeignKey("dbo.Batches", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Batches", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Batches", new[] { "DepartmentId" });
            DropColumn("dbo.Batches", "DepartmentId");
        }
    }
}
