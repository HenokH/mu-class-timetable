namespace ClassScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class timetablefinal : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Timetables", "BatchesId", "dbo.Batches");
            DropIndex("dbo.Timetables", new[] { "BatchesId" });
            DropTable("dbo.Timetables");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Timetables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BatchesId = c.Int(nullable: false),
                        WeekDay = c.String(),
                        Course = c.String(),
                        Room = c.String(),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Timetables", "BatchesId");
            AddForeignKey("dbo.Timetables", "BatchesId", "dbo.Batches", "Id", cascadeDelete: true);
        }
    }
}
