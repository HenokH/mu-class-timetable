﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassScheduler.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(User.IsInRole("Admin"))
                return View("AdminHome");

            if (User.IsInRole("DepartmentHead"))
                return View("DepartmentHeadHome");

            if (User.IsInRole("Instructors"))
                return View("InstructorsHome");

            if (User.IsInRole("Students"))
                return View("StudentsHome");

            return RedirectToRoute("Default");
        }

        
    }
}