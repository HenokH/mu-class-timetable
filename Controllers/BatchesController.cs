﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassScheduler.Models;
using ClassScheduler.ViewModels;

namespace ClassScheduler.Controllers
{
    public class BatchesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Batches
        public ActionResult Index()
        {
            return View(db.Batches.Include(d =>d.Department).ToList());
        }

        // GET: Batches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Batches batches = db.Batches.Find(id);
            if (batches == null)
            {
                return HttpNotFound();
            }
            var viewmodel = new CreateBatchViewModel
            {
                Batches = batches,
                Departments = db.Departments.ToList()
            };
            return View(viewmodel);
        }

        // GET: Batches/Create
        public ActionResult Create()
        {
            var Departments = db.Departments.ToList();
            var viewModel = new CreateBatchViewModel
            {
                Departments = Departments
        };
            return View(viewModel);
        }

        // POST: Batches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Batches batches)
        {
            if (ModelState.IsValid)
            {
                db.Batches.Add(batches);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(batches);
        }

        // GET: Batches/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Batches batches = db.Batches.Find(id);
            if (batches == null)
            {
                return HttpNotFound();
            }
            var viewmodel = new CreateBatchViewModel
            {
                Batches = batches,
                Departments = db.Departments.ToList()
            };
            return View(viewmodel);
        }

        // POST: Batches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Batches batches)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batches).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(batches);
        }

        // GET: Batches/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Batches batches = db.Batches.Find(id);
            if (batches == null)
            {
                return HttpNotFound();
            }
            
            return View(batches);
        }

        // POST: Batches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Batches batches = db.Batches.Find(id);
            db.Batches.Remove(batches);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
