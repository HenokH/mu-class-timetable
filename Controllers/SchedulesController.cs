﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassScheduler.Models;
using ClassScheduler.ViewModels;

namespace ClassScheduler.Controllers
{
    public class SchedulesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Schedules
        public ActionResult Index()
        {
            var schedules = db.Schedules.Include(s => s.Batches).Include(s => s.Course).Include(s => s.Rooms);
            return View(schedules.ToList());
        }
        public ActionResult Timetable(int? id)
        {
            
            if (id != null)
            {
                var Schedules = db.Schedules.Where(b => b.BatchesId == id).ToList();
                ViewBag.Monday = Schedules.Where(w => w.WeekDay == "Monday").ToList();
                ViewBag.Tuesday = Schedules.Where(w => w.WeekDay == "Tuesday").ToList();
                ViewBag.Wednesday = Schedules.Where(w => w.WeekDay == "Wednesday").ToList();
                ViewBag.Thursday = Schedules.Where(w => w.WeekDay == "Thursday").ToList();
                ViewBag.Friday = Schedules.Where(w => w.WeekDay == "Friday").ToList();
                ViewBag.Saturday = Schedules.Where(w => w.WeekDay == "Saturday").ToList();

            }
            return View();
        }

        public ActionResult BatchesList(int? id)
        {            if (id != null)
            {
               ViewBag.batches = db.Batches.Where(b => b.DepartmentId ==id).Include(d => d.Department).ToList();
            }
            return View();
        }
        // GET: Schedules/Details/5

        public ActionResult DepartmentsList()
        {
            return View(db.Departments.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedules schedules = db.Schedules.Find(id);
            if (schedules == null)
            {
                return HttpNotFound();
            }
            return View(schedules);
        }

        // GET: Schedules/Create
        public ActionResult Create()
        {
            ViewBag.BatchesId = new SelectList(db.Batches, "Id", "Name");
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name");
            ViewBag.RoomsId = new SelectList(db.Rooms, "Id", "Name");
            return View();
        }

        // POST: Schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BatchesId,CourseId,RoomsId,WeekDay,StartTime,EndTime")] Schedules schedules)
        {
            if (ModelState.IsValid)
            {
                db.Schedules.Add(schedules);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BatchesId = new SelectList(db.Batches, "Id", "Name", schedules.BatchesId);
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", schedules.CourseId);
            ViewBag.RoomsId = new SelectList(db.Rooms, "Id", "Name", schedules.RoomsId);
            return View(schedules);
        }

        // GET: Schedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedules schedules = db.Schedules.Find(id);
            if (schedules == null)
            {
                return HttpNotFound();
            }
            ViewBag.BatchesId = new SelectList(db.Batches, "Id", "Name", schedules.BatchesId);
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", schedules.CourseId);
            ViewBag.RoomsId = new SelectList(db.Rooms, "Id", "Name", schedules.RoomsId);
            return View(schedules);
        }

        // POST: Schedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BatchesId,CourseId,RoomsId,WeekDay,StartTime,EndTime")] Schedules schedules)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schedules).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BatchesId = new SelectList(db.Batches, "Id", "Name", schedules.BatchesId);
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", schedules.CourseId);
            ViewBag.RoomsId = new SelectList(db.Rooms, "Id", "Name", schedules.RoomsId);
            return View(schedules);
        }

        // GET: Schedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedules schedules = db.Schedules.Find(id);
            if (schedules == null)
            {
                return HttpNotFound();
            }
            return View(schedules);
        }

        // POST: Schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Schedules schedules = db.Schedules.Find(id);
            db.Schedules.Remove(schedules);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
