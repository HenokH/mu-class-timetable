﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassScheduler.Models
{
    public enum WeekDay
    {
        Monday,Tuesday,Wednesday,Thursday,Friday,Saturday
    }
    public class Schedules
    {
        public int Id { get; set; }
        public int BatchesId { get; set; }
        public int? CourseId { get; set; }
        public int RoomsId { get; set; }
        public string WeekDay { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{0:HH:mm}")]
        public DateTime StartTime { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime EndTime { get; set; }

        public virtual Batches Batches { get; set; }
        public virtual Course Course { get; set; }
        public virtual Rooms Rooms { get; set; }

    }
}