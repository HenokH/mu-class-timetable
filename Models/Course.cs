﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassScheduler.Models
{
    public class Course
    {
        public int Id { get; set; }

        public Batches Batches { get; set; }

        [Display(Name = "Department ")]
        public int BatchesId { get; set; }

        [Required]
        public string Name { get; set; }
        public string CourseCode { get; set; }
        public int CreditHour { get; set; }
        public string prerequist { get; set; }
    }
}