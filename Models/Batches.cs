﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassScheduler.Models
{
    public class Batches
    {
        public int Id { get; set; }
        public Department Department { get; set; }
        [Display(Name ="Department ")]
        public int DepartmentId { get; set; }
        [Display(Name="Batch Name")]
        public string Name { get; set; }
        public string Year { get; set; }
        public string Semester { get; set; }
        public string Section { get; set; }

        public static implicit operator Batches(List<Batches> v)
        {
            throw new NotImplementedException();
        }
    }
}