﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClassScheduler.Startup))]
namespace ClassScheduler
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
