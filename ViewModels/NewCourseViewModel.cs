﻿using ClassScheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassScheduler.ViewModels
{
    public class NewCourseViewModel
    {
        public IEnumerable<Batches> Batches { get; set; }
        public Course Course { get; set; }

    }
}