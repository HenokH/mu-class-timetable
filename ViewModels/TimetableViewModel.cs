﻿using ClassScheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassScheduler.ViewModels
{
    public class TimetableViewModel
    {
        public IEnumerable<Schedules> Schedules { get; set; }
        public IEnumerable<Batches> Batches{ get; set; }
        public IEnumerable<Rooms> Rooms { get; set; }
        public IEnumerable<Course> Courses { get; set; }

    }
}