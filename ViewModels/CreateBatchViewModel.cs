﻿using ClassScheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassScheduler.ViewModels
{
    public class CreateBatchViewModel
    {
        public IEnumerable<Department> Departments { get; set; }
        public Batches Batches { get; set; }
    }
}